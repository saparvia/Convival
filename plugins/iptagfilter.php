<?php
/// Name: IpTagFilter
/// Author: Stefan Parviainen
/// Description: Don't allow specified IP:s to add tags
/// Version: 0.1
/// Configuration: __iptagfilter_config

define ('IPTAGFILTER_CONFIG_OPTIONS', 'iptagfilter.options');

function __iptagfilter($tagstr) {
    $opts = rss_plugins_get_option( IPTAGFILTER_CONFIG_OPTIONS );
    if(empty($opts))
        return $tagstr;

    $patterns = split(' ',$opts[0]);
    foreach($patterns as $pattern) {
        $ip = $_SERVER['REMOTE_ADDR'];
	if(preg_match($pattern,$ip)) {
            return NULL;
	}
    }

    return $tagstr;
}

function __iptagfilter_config() {
    if (rss_plugins_is_submit()) {
       $opts=array();
       $opts[0]=$_REQUEST['patternlist'];

       rss_plugins_add_option(IPTAGFILTER_CONFIG_OPTIONS, $opts, 'array');
    }
    else {
        $patterns = rss_plugins_get_option( IPTAGFILTER_CONFIG_OPTIONS );
	if(!empty($patterns))
	    $patterns=$patterns[0];
	else
            $patterns="";
?>
<p>
    <input type='text' name='patternlist' id='patternlist' value='<?php echo $patterns; ?>'/>
    <label for='taglist'>Space separated list of blocked IP addresses</label>
</p>
<?php
    }
}

    rss_set_hook('rss.plugins.items.tagfilter','__iptagfilter');
?>
