<?php
// Display Image Enclosure, a plugin for Gregarius
// Copyright (C) 2006  Ludovic Perrine <jazz@fr.fm>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Based upon auto-highlighter by Charilaos Skiadas

/// Name: Display filter
/// Author: Ludovic Perrine
/// Description: Find and replace regexps in items.
/// Version: 0.2
/// Configuration: _displayfilter_config

// History:
//
// 0.1: Initial release
// 0.2: Fixed bug

define ('DF_CONFIG_REGEXPS','df.regexps');

function _displayfilter_config()
{
  if (rss_plugins_is_submit()) {
    $keyword_array = array();
    foreach($_REQUEST as $key => $value) {
      $newkey = str_replace('df_pattern_input','df_replacement_input',$key);
      if ($newkey != $key && $newkey != null && array_key_exists($newkey, $_REQUEST)) {
	$keyword_array[$value] = $_REQUEST[$newkey];
      }
    }
    rss_plugins_add_option(DF_CONFIG_REGEXPS, $keyword_array, 'array');
    return;
  }

  $keyword_array = rss_plugins_get_option(DF_CONFIG_REGEXPS);
  if($keyword_array == null || !is_array($keyword_array))
    {
      $keyword_array = array("" => "");
    }
?>
<script type="text/javascript">
<!--
function __df_removeitem(node) {
  var df_FormsChildren = node.parentNode.parentNode.childNodes;
  var df_currentNum = 0;
  var df_tempNode = df_FormsChildren[0];
  for (i=0; i < df_FormsChildren.length; i++)
  {
    if (df_tempNode.nodeType == 1 && df_tempNode.nodeName == 'DIV') df_currentNum = df_currentNum + 1;
    df_tempNode = df_tempNode.nextSibling;
  }
  if (df_currentNum > 1) node.parentNode.parentNode.removeChild(node.parentNode);
}
function __df_additem() {
	var df_formsContainer = document.getElementById('df_formsContainer');
	var df_lastForm = df_formsContainer.lastChild;
	while (df_lastForm.nodeType != 1 || df_lastForm.nodeName != 'DIV') df_lastForm = df_lastForm.previousSibling;
	var df_newNode = df_lastForm.cloneNode(true);
	var df_patternNode = df_newNode.firstChild;
	while (df_patternNode.nodeType != 1 || df_patternNode.nodeName != 'INPUT') df_patternNode = df_patternNode.nextSibling;
	var df_replacementNode = df_patternNode.nextSibling;
	while (df_replacementNode.nodeType != 1 || df_replacementNode.nodeName != 'INPUT') df_replacementNode = df_replacementNode.nextSibling;
	var df_newKeywordNumber = parseInt(df_patternNode.name.substring(16)) + 1;
	df_patternNode.setAttribute('name', 'df_pattern_input' + df_newKeywordNumber);
	df_patternNode.setAttribute('id', 'df_pattern_input' + df_newKeywordNumber);
	df_patternNode.value = "";
	df_replacementNode.setAttribute('name', 'df_replacement_input' + df_newKeywordNumber);
	df_replacementNode.setAttribute('id', 'df_replacement_input' + df_newKeywordNumber);
	df_replacementNode.value = "";
	df_newNode.setAttribute('id','df_form' + df_newKeywordNumber);
	df_formsContainer.insertBefore(df_newNode,df_lastForm.nextSibling);
}
-->
</script>
<?php
	echo "<div id=\"df_formsContainer\">\n";
	echo "<input type=\"button\" onclick=\"javascript:__df_additem()\" value=\"Add pair\" /><br />\n";
	$count = 0;
	foreach($keyword_array as $pattern => $replacement)
	{
	  if ($replacement == null) {
	    $replacement = "";
	  }
	  echo "<div id=\"df_form$count\" class=\"form\">\n<label for=\"df_pattern_input$count\">Pattern:</label>" . "<input name=\"df_pattern_input$count\" id=\"df_pattern_input$count\" type=\"text\" value=\"".htmlentities($pattern)."\" />\n";
	  echo "<label for=\"df_replacement_input$count\">Replacement:</label>"."<input name=\"df_replacement_input$count\" type=\"text\" value=\"".htmlentities($replacement)."\" /><input type=\"button\" onclick=\"javascript:__df_removeitem(this)\" value=\"Remove\" /><br /></div>\n";
	  $count++;
	}
	echo "<input type=\"button\" onclick=\"javascript:__df_additem()\" value=\"Add pair\" />\n";
	echo "</div>\n";
	return;
}

function __displayfilter_rewrite($item)
{
  $regexps_array = rss_plugins_get_option(DF_CONFIG_REGEXPS);
  foreach($regexps_array as $regexp => $replacement) {
    if ( $regexp != null && $replacement != null ) {
      $item->description =
	ereg_replace( $regexp,
		      $replacement,
		      $item->description );
    }
  }
  return $item;
}

rss_set_hook('rss.plugins.items.beforerender','__displayfilter_rewrite');

?>