<?php
/// Name: NoUnreadItems
/// Author: Sameer D'Costa
/// Description: Marks items as read as soon as they come in. 
/// Version: 0.1

function __NoUnreadItems()
{
    rss_plugins_set_item_state("", RSS_MODE_UNREAD_STATE, 0, "", true);
}

rss_set_hook('rss.plugins.updates.after','__NoUnreadItems');
?>
