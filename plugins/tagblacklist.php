<?php
/// Name: TagBlacklist 
/// Author: Stefan Parviainen
/// Description: Remove offending tags
/// Version: 0.1
/// Configuration: __tagblacklist_config

define ('TAGBLACKLIST_CONFIG_OPTIONS', 'tagblacklist.options');

function __tagblacklist($tagstr) {
    $opts = rss_plugins_get_option( TAGBLACKLIST_CONFIG_OPTIONS );
    if(empty($opts))
        return $tagstr;

    $tags = split(' ',$opts[0]);
    $tagstr = $tagstr.' ';
    foreach($tags as $tag) {
        $tagstr=preg_replace("/$tag /", '', $tagstr);
    }

    return trim($tagstr);
}

function __tagblacklist_config() {
    if (rss_plugins_is_submit()) {
       $opts=array();
       $opts[0]=$_REQUEST['taglist'];

       rss_plugins_add_option(TAGBLACKLIST_CONFIG_OPTIONS, $opts, 'array'); 
    }
    else {
        $tags = rss_plugins_get_option( TAGBLACKLIST_CONFIG_OPTIONS );
	if(!empty($tags))
	    $tags=$tags[0];
	else
            $tags="";
?>
<p>
    <input type='text' name='taglist' id='taglist' value='<?php echo $tags; ?>'/>
    <label for='taglist'>Space separated list of forbidden tags</label>
</p>
<?php
    }
}

    rss_set_hook('rss.plugins.items.tagblacklist','__tagblacklist');
?>
